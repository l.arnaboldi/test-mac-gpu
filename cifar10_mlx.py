import torchvision
import numpy as np
import mlx.nn as nn

BATCH_SIZE = 1024
TEST_BATCH_SIZE = 1024
LEARNING_RATE = 0.01
EPOCHS = 10

def prepare_dataset(dataset):
    new_set = []
    for (X,Y) in dataset:
        y = np.zeros(10, dtype=np.float32)
        y[Y] = 1.
        new_set.append((
            X.astype(np.float32),
            y
        ))
    return new_set

class CifarCNN(nn.Module):
    def __init__(self):
        super().__init__()

        # We use a Sequential, i.e. the inputs passes through each of
        # the modules below, one-by-one
        self.conv = nn.Sequential(
            nn.Conv2d(
                in_channels=3, #COLORED IMAGES!
                out_channels=16,
                kernel_size=3,
                stride=1,
                padding=1,
            ),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2),
            nn.Conv2d(
                in_channels=16,
                out_channels=32,
                kernel_size=3,
                stride=1,
                padding=1),
            nn.ReLU(),
            nn.MaxPool2d(2),
        )

        # fully connected layer, output 10 classes
        self.out = nn.Linear(2048, 10) #Now is 32x32 images!

    def forward(self, x):
        x = self.conv(x)
        x = x.view(x.size(0), -1)
        x = self.out(x)
        return x


if __name__ == "__main__":
    transform = np.asarray

    # load the train dataset
    train_dataset = torchvision.datasets.CIFAR10(
        root='./data/',
        train=True,
        download=True,
        transform=transform)

    # load the test dataset
    test_dataset = torchvision.datasets.CIFAR10(
        root='./data/',
        train=False,
        download=True,
        transform=transform)
    
    print(type(train_dataset))
    
    train_dataset = prepare_dataset(train_dataset)
    test_dataset = prepare_dataset(test_dataset)