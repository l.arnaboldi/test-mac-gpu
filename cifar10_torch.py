import torch
import torch.nn as nn
import torchvision
from torch.utils.data import DataLoader
import torch.nn.functional as F
import time

def main(device):
    BATCH_SIZE = 1024
    TEST_BATCH_SIZE = 1024
    LEARNING_RATE = 0.01
    EPOCHS = 10
    DEVICE = torch.device(device)

    def train_epoch(
        model: nn.Module,
        train_dataloader: DataLoader,
        optimizer: torch.optim.Optimizer,
        device: torch.device,
    ):
        """
        This function implements the core components of any Neural Network training regiment.
        In our stochastic setting our code follows a very specific "path". First, we load the batch
        a single batch and zero the optimizer. Then we perform the forward pass, compute the gradients and perform the backward pass. And ...repeat!
        """

        running_loss = 0.0
        model = model.to(device)
        model.train()
        for batch_idx, (data, target) in enumerate(train_dataloader):
            # move data and target to device
            data, target = data.to(device), target.to(device)

            # zero the parameter gradients
            optimizer.zero_grad()

            # do the forward pass
            output = model(data)

            # compute the loss
            loss = F.cross_entropy(output, target)

            # compute the gradients
            loss.backward()

            # perform the gradient step
            optimizer.step()

            # print statistics
            running_loss += loss.item()

        return running_loss / len(train_dataloader)


    def fit(
        model: nn.Module,
        train_dataloader: DataLoader,
        optimizer: torch.optim.Optimizer,
        epochs: int,
        device: torch.device,
    ):
        """
        the fit method simply calls the train_epoch() method for a
        specified number of epochs.
        """

        # keep track of the losses in order to visualize them later
        losses = []
        for epoch in range(epochs):
            print(f"Epoch {epoch}...")
            start = time.time()
            running_loss = train_epoch(
                model=model,
                train_dataloader=train_dataloader,
                optimizer=optimizer,
                device=device,
            )
            print(f"Epoch {epoch}: Loss={running_loss}, Time={time.time() - start}")
            losses.append(running_loss)

        return losses


    def predict(
        model: nn.Module, test_dataloader: DataLoader, device: torch.device, verbose=True
    ):
        model.eval()
        test_loss = 0
        correct = 0
        with torch.no_grad():
            for data, target in test_dataloader:
                data, target = data.to(device), target.to(device)
                output = model(data)
                loss = F.cross_entropy(output, target, reduction="sum")
                test_loss += loss.item()
                pred = output.data.max(1, keepdim=True)[1]
                correct += pred.eq(target.data.view_as(pred)).sum()

        test_loss /= len(test_dataloader.dataset)
        accuracy = 100.0 * correct / len(test_dataloader.dataset)

        if verbose:
            print(
                f"Test set: Avg. loss: {test_loss:.4f}, Accuracy: {correct}/{len(test_dataloader.dataset)} ({accuracy:.0f}%)"
            )

        return test_loss, accuracy


    class CifarCNN(nn.Module):
        def __init__(self):
            super().__init__()

            # We use a Sequential, i.e. the inputs passes through each of
            # the modules below, one-by-one
            self.conv = nn.Sequential(
                nn.Conv2d(
                    in_channels=3, #COLORED IMAGES!
                    out_channels=16,
                    kernel_size=3,
                    stride=1,
                    padding=1,
                ),
                nn.ReLU(),
                nn.MaxPool2d(kernel_size=2),
                nn.Conv2d(
                    in_channels=16,
                    out_channels=32,
                    kernel_size=3,
                    stride=1,
                    padding=1),
                nn.ReLU(),
                nn.MaxPool2d(2),
            )

            # fully connected layer, output 10 classes
            self.out = nn.Linear(2048, 10) #Now is 32x32 images!

        def forward(self, x):
            x = self.conv(x)
            x = x.view(x.size(0), -1)
            x = self.out(x)
            return x

    
    transform = torchvision.transforms.ToTensor()

    # load the train dataset
    train_dataset = torchvision.datasets.CIFAR10(
        root='./data/',
        train=True,
        download=True,
        transform=transform)

    # load the test dataset
    test_dataset = torchvision.datasets.CIFAR10(
        root='./data/',
        train=False,
        download=True,
        transform=transform)

    train_dataloader = DataLoader(
        dataset=train_dataset,
        batch_size=BATCH_SIZE,
        shuffle=True,
        num_workers=2)


    test_dataloader = DataLoader(
        dataset=test_dataset,
        batch_size=TEST_BATCH_SIZE,
        shuffle=False,
        num_workers=2)
    
    cnn_cifar = CifarCNN().to(DEVICE)

    # define the optimizer
    optimizer = torch.optim.SGD(cnn_cifar.parameters(), lr=0.1)


    # train the CNN
    losses = fit(
        model=cnn_cifar,
        train_dataloader=train_dataloader,
        optimizer=optimizer,
        epochs=EPOCHS,
        device=DEVICE
    )

    predict(model=cnn_cifar, test_dataloader=test_dataloader, device=DEVICE)

if __name__ == "__main__":
    print("Running on GPU...")
    main('mps')
    print("Running on CPU...")
    main('cpu')